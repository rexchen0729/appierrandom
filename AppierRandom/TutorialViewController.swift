//
//  TutorialViewController.swift
//  AppierRandom
//
//  Created by chunta on 2020/7/21.
//  Copyright © 2020 A. All rights reserved.
//

import Foundation

import UIKit

class TutorialViewController: UIViewController {
    
    @IBOutlet var tableView:UITableView!
    
    @IBOutlet var pickBtn:UIButton!
    
    @IBOutlet var understandBtn:UIButton!
    
    let defaultOptions:[String] = ["Pad Thai", "Green Curry Chicken",
                                   "Burger KING - WHOPPER Meal",
                                   "McDonald - Big Mac Meal",
                                   "McDonald - Quarter Pounder with Cheese Meal",
                                   "Pork Fried Rice"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Tutorial"
        
        pickBtn.layer.cornerRadius = 2
        pickBtn.layer.borderWidth = 1
        pickBtn.layer.borderColor = UIColor.darkGray.cgColor
        
        understandBtn.layer.cornerRadius = 2
        understandBtn.layer.borderWidth = 1
        understandBtn.layer.borderColor = UIColor.darkGray.cgColor
        
        tableView.delegate = self
        tableView.dataSource = self
        
        /*
        let customView:uI
        let button = UIButton(type: UIButton.ButtonType.custom)
        button.setImage(UIImage(named: "cross"), for: .normal)
        button.addTarget(self, action: #selector(onDismiss), for: .touchDown)
        button.frame = CGRect(x: 0, y: 0, width: 26, height: 26)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItems = [barButton]
        */
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        //customView.layer.borderWidth = 1;
        let btnSearch = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        btnSearch.imageEdgeInsets = UIEdgeInsets(top: 6, left: 12, bottom: 6, right: 0)
        btnSearch.setImage(UIImage(named: "cross"), for: .normal)
        btnSearch.addTarget(self, action: #selector(onDismiss), for: .touchDown)
        //btnSearch.layer.borderWidth = 1;
        customView.addSubview(btnSearch)
        /*
        let btnCart = UIButton(frame: CGRect(x: 40, y: 0, width: 40, height: 40))
        btnCart.imageEdgeInsets = UIEdgeInsets(top: 6, left: 12, bottom: 6, right: 0)
        btnCart.setImage(UIImage(named: "cart_icon"), for: .normal)
        btnCart.addTarget(self, action: #selector(onRightTopBarCartClicked(_:)), for: .touchUpInside)
        customView.addSubview(btnCart)
        */
        let rightItem = UIBarButtonItem(customView: customView)
        navigationItem.rightBarButtonItem = rightItem

    }

    @objc func onDismiss() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onRandomPick(sender:Any) {
        if (defaultOptions.count > 0) {
            let randomSelIndex:Int = Int.random(in: 0 ..< defaultOptions.count)
            tableView .selectRow(at: IndexPath.init(row: randomSelIndex, section: 0), animated: true, scrollPosition: .top)
        }
    }
    
    @IBAction func onNotShowAtStartup(sender:Any) {
        PreferenceMng.NotShowTutorialAtStartUp()
        onDismiss()
    }
}

extension TutorialViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return defaultOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        if( !(cell != nil))
        {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "Cell")
        }
        cell!.textLabel?.text = defaultOptions[indexPath.row]
        return cell!
    }
}
