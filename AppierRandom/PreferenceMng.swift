//
//  PreferenceMng.swift
//  AppierRandom
//
//  Created by chunta on 2020/7/21.
//  Copyright © 2020 A. All rights reserved.
//

import Foundation
class PreferenceMng {
    public class func saveCustomOption(options:[String]) {
        UserDefaults.standard.set(options, forKey: "options")
        UserDefaults.standard.synchronize()
    }
    
    public class func getCustomOptions()->[String] {
        let options:[String]? = UserDefaults.standard.array(forKey: "options") as? [String]
        if options == nil {
            return []
        }
        return options!
    }
    
    public class func isNotShowTutorialAtStartUp()->Bool {
        let notshow:Bool? = UserDefaults.standard.bool(forKey: "notshowatstartup")
        if notshow == nil {
            return false
        } else {
            return notshow!
        }
    }
    
    public class func NotShowTutorialAtStartUp() {
        UserDefaults.standard.set(true, forKey: "notshowatstartup")
    }
}
