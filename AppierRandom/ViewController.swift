//
//  ViewController.swift
//  AppierRandom
//
//  Created by chunta on 2020/7/21.
//  Copyright © 2020 A. All rights reserved.
//

import UIKit
import StoreKit
//import AppTrackingTransparency

class ViewController: UIViewController {

    @IBOutlet var tableView:UITableView!
    
    @IBOutlet var addBtn:UIButton!
    
    @IBOutlet var infoBtn:UIButton!
    
    @IBOutlet var pickBtn:UIButton!
    
    @IBOutlet var inputTxt:UITextField!
    
    var customOptions:[String] =  []
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // send conversion
        //self.updateConversionValue()
        
        addBtn.layer.cornerRadius = 2
        addBtn.layer.borderWidth = 1
        addBtn.layer.borderColor = UIColor.darkGray.cgColor
        infoBtn.layer.cornerRadius = 2
        infoBtn.layer.borderWidth = 1
        infoBtn.layer.borderColor = UIColor.darkGray.cgColor
        pickBtn.layer.cornerRadius = 2
        pickBtn.layer.borderWidth = 1
        pickBtn.layer.borderColor = UIColor.darkGray.cgColor
        
        tableView.delegate = self
        tableView.dataSource = self
        
        inputTxt.delegate = self

        customOptions = PreferenceMng.getCustomOptions()
        
        if PreferenceMng.isNotShowTutorialAtStartUp() == false {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.presentTutorialViewController()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //dialog for consent
        //self.showTrackingConsentDialog()
    }
    
//    // App Tracking and SKAdnetwork conversion
//    func showTrackingConsentDialog() {
//        // Checking the OS version before calling the Tracking Consent dialog, it's available only in iOS 14 and above
//        if #available(iOS 14, *) {
//            if ATTrackingManager.trackingAuthorizationStatus == .denied {
//                // consent dialog alreayd shown
//                alertTrackingConsentIsAlreadySet()
//            }
//
//            ATTrackingManager.requestTrackingAuthorization(completionHandler: { status in
//                // status after consent dialog
//                print(status)
//                self.updateConversionValue()
//            })
//        } else {
//            // No need to display the dialog in earlier versions
//        }
//    }
//
//
//    func updateConversionValue() {
//        if #available(iOS 14, *) {
//            SKAdNetwork.updateConversionValue(3)
//        } else {
//            // Fallback on earlier versions
//        }
//    }
    
    func presentTutorialViewController() {
        let tutorialViewController: TutorialViewController = TutorialViewController.init()
        let navEditorViewController: UINavigationController = UINavigationController(rootViewController: tutorialViewController)
        self .present(navEditorViewController, animated: true, completion: nil)
    }
    
    @IBAction func onRandomPick(sender:Any) {
        if (customOptions.count > 0) {
            let randomSelIndex:Int = Int.random(in: 0 ..< customOptions.count)
            tableView .selectRow(at: IndexPath.init(row: randomSelIndex, section: 0), animated: true, scrollPosition: .top)
        }
    }
    
    @IBAction func onTutorial(sender:Any) {
        presentTutorialViewController()
    }
    
    @IBAction func onAddItem(sender:Any) {
        addItem()
    }
    
    func addItem() {
        if let txt:String = inputTxt.text {
            if (txt.count > 0) {
                customOptions.append(txt)
                PreferenceMng.saveCustomOption(options: customOptions)
                tableView.reloadData()
                inputTxt.text = ""
            } else {
                print("no text input")
            }
        }
    }
    
    func alertTrackingConsentIsAlreadySet() {
        let alert : UIAlertController = UIAlertController.init(title: "User Tracking Disabled", message: "Can't display Tracking Consent dialog because it has already been displayed. \n To Allow tracking and show dialog, please remove and reinstall this app", preferredStyle: .alert)
        let defaultAction = UIAlertAction.init(title: "OK", style: .default, handler: nil)
        alert.addAction(defaultAction)
        self .present(alert, animated: true, completion: nil)
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return customOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        if( !(cell != nil))
        {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "Cell")
        }
        cell!.textLabel?.text = customOptions[indexPath.row]
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.init(red: 1, green: 1, blue: 0, alpha: 0.7)
        cell!.selectedBackgroundView = backgroundView
        return cell!
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            customOptions.remove(at: indexPath.row)
            PreferenceMng.saveCustomOption(options: customOptions)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
}

extension ViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
         textField.resignFirstResponder()
         return true
    }
}
