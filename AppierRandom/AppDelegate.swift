//
//  AppDelegate.swift
//  AppierRandom
//
//  Created by chunta on 2020/7/21.
//  Copyright © 2020 A. All rights reserved.
//

import UIKit
import StoreKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        if #available(iOS 11.3, *) {
            SKAdNetwork.registerAppForAdNetworkAttribution()
        } else {
            // Fallback on earlier versions
        }
        
        return true
    }
}

